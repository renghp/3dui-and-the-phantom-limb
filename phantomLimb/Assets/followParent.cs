﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followParent : MonoBehaviour
{

    public Transform palm;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       // bool status = leftHand.activeSelf;
        transform.GetChild(0).gameObject.SetActive(palm.gameObject.activeInHierarchy);

        transform.position = palm.position;
        transform.rotation = palm.rotation;
    }
}
