﻿namespace exiii.Unity
{
#pragma warning disable CS1591 // 公開されている型またはメンバーの XML コメントがありません

    /// <summary>
    /// Type of LR
    /// </summary>
    public enum ELRType
    {
        Right,
        Left,
        None,
    }

#pragma warning restore CS1591 // 公開されている型またはメンバーの XML コメントがありません
}