﻿namespace exiii.Unity.Connection
{
#pragma warning disable CS1591 // 公開されている型またはメンバーの XML コメントがありません

    /// <summary>
    /// Type of Connection
    /// </summary>
    public enum EConnectionType
    {
        Osc,
        Serial,
    }

    /// <summary>
    /// Type of Osc dll
    /// </summary>
    public enum EOscType
    {
        None,
        SharpOSC,
    }

#pragma warning restore CS1591 // 公開されている型またはメンバーの XML コメントがありません
}