﻿using exiii.Unity.Device;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmCollison : MonoBehaviour
{
    [SerializeField]
    private ExosDevice m_Device;

    void Start()
    {

        if (m_Device == null)
        {
            return;
        }

        if (!m_Device.Enabled)
        {
            m_Device.Initialize();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("Colliding");
        applyForceOnExos(-1.0f);        
    }

    private void OnCollisionExit(Collision collision)
    {
        //Debug.Log("Exiting");
        applyForceOnExos(0.0f);
    }

    private void applyForceOnExos(float force)
    {
        ExosJoint joint_abduction = m_Device.GetJoint(EAxisType.Abduction);
        ExosJoint joint_flextion = m_Device.GetJoint(EAxisType.Flexion);

        if (joint_abduction == null || joint_flextion == null)
        {
            return;
        }
        
        joint_flextion.ForceRatio = force;
    }

    void Update()
    {
        //
    }



    public void OnDestroy()
    {
        if (m_Device.Enabled)
        {
            m_Device.Termination();
        }
    }
}
